<?php

namespace Dipper\Qrcode;

class Qrcode
{
    public static function decode($imgSource, $sourceType = QrReader::SOURCE_TYPE_FILE)
    {
        $qrcode = new QrReader($imgSource, $sourceType);
        return $qrcode->text();
    }
}
