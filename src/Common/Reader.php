<?php

namespace Dipper\Qrcode\Common;

interface Reader
{
    public function decode(BinaryBitmap $image);

    public function reset();
}
