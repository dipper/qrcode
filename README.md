# QR code decoder / reader for PHP
This is a PHP library to detect and decode QR-codes.<br />This is first and only QR code reader that works without extensions.<br />

## Requirements 
* PHP >= 5.6
* GD Library

## Installation
The recommended method of installing this library is via [Composer](https://getcomposer.org/).

Run the following command from your project root:

```bash
$ composer require dipper/qrcode
```

## Usage 
```php
use Dipper\Qrcode\Qrcode;
$text = Qrcode::decode('path/to_image'); //return decoded text from QR Code

use Dipper\Qrcode\QrReader;
$qrcode = new QrReader('path/to_image');
$text = $qrcode->text(); //return decoded text from QR Code
```
