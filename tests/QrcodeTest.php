<?php

namespace Dipper\Qrcode\Tests;

use Dipper\Qrcode\Qrcode;

class QrcodeTest extends TestCase
{
    public function testEncode()
    {
        $image = __DIR__ . "/qrcodes/hello_world.png";

        $result = Qrcode::decode($image);

        $this->assertSame("Hello world!", $result);
    }
}
